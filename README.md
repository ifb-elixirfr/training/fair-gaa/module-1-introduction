# Module 1 - Introduction générale

## Description Cours Magistral (CM)

⏱️ Durée : 3h

📑 Plan :
- Présentation des objectifs de l’UE
  - Objectifs généraux
  - Présentation des modules et intervenant·e·s
  - Planning des 2 semaines
  - Evaluations
- Présentation des principes FAIR
- Présentation des concepts pour l’assemblage et l’annotation de génomes
- Ressources addititionnelles

📚 Supports : 
- [slides](https://ifb-elixirfr.gitlab.io/training/fair-gaa/module-1-introduction)